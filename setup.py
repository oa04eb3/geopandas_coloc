from setuptools import setup

setup(name='geopandas-coloc',
      description='colocalize two geodataframe',
      url='https://gitlab.ifremer.fr/oa04eb3/geopandas_coloc',
      author = "Olivier Archer",
      author_email = "Olivier.Archer@ifremer.fr",
      license='GPL',
      packages=['geopandas_coloc'],
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      zip_safe=False,
      install_requires=[ 'geopandas', 'pandas', 'tqdm' ],
)
